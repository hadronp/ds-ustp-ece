# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This contains the presentation slides and demo and exercises JupyterLab notebooks and public datasets used in the training session for USTP ENGG faculty on 07 and 14 December 2019.

## Slides ###

* navigate to the ```slides``` directory
* Open index.html on your preferred browser ()

### Demo ###

* Install conda for your platform https://conda.io/miniconda.html
* Clone this repository
* navigate to the ```ds-training-ece``` directory
* in the CLI type : ```conda create --name ds-training-ece pip python=3.7```
* to activate the virtual environment type: ```source activate ds-training-ece```
* Install packages and dependencies: ```pip install -r requirements.txt```
* run the JupyterLab notebook:  ```jupyter lab```
* open the notebook in the browser explorer 

### Who do I talk to? ###

* Repo owner or admin https://www.facebook.com/aryan.limjap
* https://www.facebook.com/groups/itgpytsada/
