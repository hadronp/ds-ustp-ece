{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python and Pandas \n",
    "\n",
    "\n",
    "\n",
    "## The importance of data preprocessing\n",
    "\n",
    "Data preprocessing (also called data wrangling, cleaning, scrubbing, etc) is the most important thing you will do with your data because it sets the stage for the analysis part of your data analysis workflow. The preprocessing you do largely depends on what kind of data you have, what sort of analysis you'll be doing with your data, and what you intend to do with the results.\n",
    "\n",
    "Preprocessing is also a process for getting to know your data, and can answer questions such as these (and more): \n",
    "\n",
    "- What kind of data are you working with? \n",
    "- Is it categorical, continuous, or a mix of both? \n",
    "- What's the distribution of features in your dataset? \n",
    "- What sort of wrangling do you have to do?\n",
    "- Do you have any missing data? \n",
    "- Do you need to remove missing data?\n",
    "- Do you need only a subset of your data?\n",
    "- Do you need more data?\n",
    "- Or less?\n",
    "\n",
    "The questions you'll have to answer are, again, dependent upon the data that you're working with, and preprocessing can be a way to figure that out.\n",
    "\n",
    "## What is Pandas?\n",
    "\n",
    "Pandas is by far my favorite preprocessing tool. It's a data wrangling/modeling/analysis tool that is similar to R and Excel; in fact, the DataFrame data structure in Pandas was named after the DataFrame in R. Pandas comes with several easy-to-use data structures, two of which (the `Series` and the `DataFrame`) I'll be covering here.\n",
    "\n",
    "I'll also be covering a bunch of different wrangling tools, as well as a couple of analysis tools.\n",
    "\n",
    "## Why Pandas?\n",
    "\n",
    "So, why would you want to use Python, as opposed to tools like R and Excel? I like to use it because I like to keep everything in Python, from start to finish. It just makes it easier if I don't have to switch back and forth between other tools. Also, if I have to build in preprocessing as part of a production system, which I've had to do at my job, it makes sense to just do it in Python from the beginning. \n",
    "\n",
    "Pandas is great for preprocessing, as we'll see, and it can be easily combined with other modules from the scientific Python stack.\n",
    "\n",
    "## Pandas data structures\n",
    "\n",
    "Pandas has several different data structures, but we're going to talk about the `Series` and the `DataFrame`.\n",
    "\n",
    "### The Series\n",
    "\n",
    "The `Series` is a one-dimensional array that can hold a variety of data types, including a mix of those types. The row labels in a `Series` are collectively called the index. You can create a `Series` in a few different ways. Here's how you'd create a `Series` from a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "some_numbers = [2, 5, 7, 3, 8]\n",
    "\n",
    "series_1 = pd.Series(some_numbers)\n",
    "series_1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To specify an index, you can also pass in a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ind = ['a', 'b', 'c', 'd', 'e']\n",
    "\n",
    "series_2 = pd.Series(some_numbers, index=ind)\n",
    "series_2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can pull that index back out again, too, with the `.index` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "series_2.index"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also create a `Series` with a dictionary. The keys of the dictionary will be used as the index, and the values will be used as the `Series` array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "more_numbers = {'a': 9, 'b': 'eight', 'c': 7.5, 'd': 6}\n",
    "\n",
    "series_3 = pd.Series(more_numbers)\n",
    "series_3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how, in that previous example, We created a `Series` with integers, a float, and a string."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The DataFrame\n",
    "\n",
    "The `DataFrame` is Pandas' most used data structure. It's a two and greater dimensional structure that can also hold a variety of mixed data types. It's similar to a spreadsheet in Excel or a SQL table. You can create a `DataFrame` with a few different methods. First, let's look at how to create a `DataFrame` from multiple `Series` objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "combine_series = pd.DataFrame([series_2, series_3])\n",
    "combine_series"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how in column `b`, we have two kinds of data. If a column in a `DataFrame` contains multiple types of data, the data type (or `dtype`) of the column will be chosen to accomodate all of the data. We can look at the data types of different columns with the `.dtypes` attribute. `object` is the most general, which is what has been chosen for column `b`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "combine_series.dtypes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another way to create a `DataFrame` is with a dictionary of lists. This is pretty straightforward:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = {'col1': ['i', 'love', 'pandas', 'so', 'much'],\n",
    "        'col2': ['so', 'will', 'you', 'i', 'promise']}\n",
    "\n",
    "df = pd.DataFrame(data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## File I/O\n",
    "\n",
    "It's really easy to read data into Pandas from a file. Pandas will read your file directly into a `DataFrame`. There are multiple ways to read in files, but they all work in the same way. Here's how you read in a CSV file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df = pd.read_csv('../dataset/titanic.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Here's a quick description of each column in the DataFrame:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `head` and `tail` function allows examining the last 5 rows in a dataframe. Moreover value `x` can be passed as parameter to both functions to specify the `x` number of rows to return from the dataframe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.tail()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Here's a quick description of each column in the DataFrame:\n",
    "*    ```PassengerId```- Passenger ID\n",
    "*    ```Survived``` - Survival (0 = No; 1 = Yes)\n",
    "*    ```PClass``` - Passenger Class (1 = 1st; 2 = 2nd; 3 = 3rd)\n",
    "*    ```Name``` - Name\n",
    "*    ```Sex``` - Sex\n",
    "*    ```Age``` - Age\n",
    "*    ```SibSp``` - Number of Siblings/Spouses Aboard\n",
    "*    ```Parch``` - Number of Parents/Children Aboard\n",
    "*    ```Ticket``` - Ticket Number\n",
    "*    ```Fare``` - Passenger Fare\n",
    "*    ```Cabin``` - Cabin\n",
    "*    ```Embarked``` - Port of Embarkation (C = Cherbourg; Q = Queenstown; S = Southampton)\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exploring the data\n",
    "\n",
    "Here are some different ways to explore the data we have. Let's first take a look at some of the basic characteristics of the `titanic_df` dataset. You can easily find the number of rows and the number of columns a dataframe has using the `.shape` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Getting column names from a `DataFrame` is also easy and can be done using the `.columns` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.dtypes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before applying summary statistics to the dataframe, we need to fix several columns that are considered categorical variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df['Survived'] = pd.Categorical(titanic_df.Survived)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df['Sex'] = pd.Categorical(titanic_df.Sex)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df['Pclass'] = pd.Categorical(titanic_df.Pclass)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df['Embarked'] = pd.Categorical(titanic_df.Embarked)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.dtypes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another useful thing you can do is generate some summary statistics using the `describe()` function. The `describe()` function calculates descriptive statistics like the mean, standard deviation, and quartile values for continuous and integer data that exist in your dataset. Don't worry, Pandas won't try to calculate the standard deviation of your categorical values!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another useful thing you can do to explore your data is to sort it. Let's say we wanted to sort our `titanic_df` DataFrame by `Age`. This is very easy as well: (default is ascending order)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.sort_values(by='Age').head(10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.sort_values(by='Age', ascending=False).head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Working with dataframes\n",
    "\n",
    "Pandas has a ton of functionality for manipulating and wrangling the data. Let's look at a bunch of different ways to select and subset our data.\n",
    "\n",
    "### Selecting columns and rows\n",
    "\n",
    "There are multiple ways to select by both rows and columns. From index to slicing to label to position, there are a variety of methods to suit your data wrangling needs.\n",
    "\n",
    "Let's select just the `Embarked` column from the `titanic_df` DataFrame. This works similar to how you would access values from a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df['Embarked']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can do exactly the same thing by using `Embarked` as an attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.Embarked"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To extract rows from a `DataFrame`, you can use the slice method, similar to how you would slice a list. Here's how we would grab rows 7-13 from the `titanic_df` DataFrame:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df[7:14]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One convenient method for selecting data in Pandas is through boolean indexing. Boolean indexing is similar to the WHERE clause in SQL in that it allows you to filter out data based on certain criteria. Let's see how this works.\n",
    "\n",
    "Let's select from the `titanic_df` DataFrame where `Pclass` is 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df[titanic_df['Pclass'] == 1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This works with any comparison operators, like >, < >=, !=, and so on. For example, we can select everything from the `titanic_df` DataFrame where the value in the `Age` column is less than 15."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df[titanic_df['Age'] < 15]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also say 'not' with the tilde: ~\n",
    "\n",
    "Let's select from the `titanic_df` DataFrame` where `Age` is NOT greater than 40, which is equivalent to saying less than or equal to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df[~(titanic_df['Age'] > 40)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Show how the wealthy passengers fared in the voyage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's also possible to combine these boolean indexers. Make sure you enclose them in parentheses. This is something I usually forget.\n",
    "\n",
    "Let's select from `titanic_df` where `Pclass` is 1 and `Survived` is 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df[(titanic_df['Pclass'] == 1) & (titanic_df['Survived'] == 0)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Groupby\n",
    "\n",
    "`groupby()` is just like SQL's 'group by' clause. What ```groupby``` does is a three-step process:\n",
    "\n",
    "- Split the data\n",
    "- Apply a function to the split groups\n",
    "- Recombine the data\n",
    "\n",
    "In the apply step, you can do things like apply a statistical function, filter out data, or transform the data.\n",
    "\n",
    "Let's `groupby()` the `Pclass` in our  `titanic_df` DataFrame! Let's start with just `groupby()`, and then build it from there. This will produce a `DataFrame groupby` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.groupby('Pclass')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not so interesting yet. This object has some attributes you can access. We can get lists of which rows are in which group by using the `.groups` attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.groupby('Pclass').groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Expanding further the `titanic_df` example, let's apply an aggregate function. Let's generate the mean of all the other values and group them by `Pclass`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.groupby('Pclass').mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's also possible to apply multiple functions to the entire `DataFrame` using the `agg()` function. Let's get not only the mean, but the count and the standard deviation as well for each value in the `DataFrame`, still grouping by `Pclass`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic_df.groupby('Pclass').agg(['mean', 'count', 'std'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pivoting\n",
    "\n",
    "You can pivot in Pandas just like you would in Excel. `pivot_table()` takes in four requires parameters: the `DataFrame`, the column to use for the index, the column to use for the columns, and the column to use for the values. `pivot_table()` also has an `aggfunc` parameter that defaults to the mean of the values, but you can pass in other functions, just as we did in the `agg()` function before.\n",
    "\n",
    "#### Let's look at the mean `Age` per `Pclass` and `Sex` combination."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.pivot_table(titanic_df, values='Age', index='Pclass', columns='Sex')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Alternative library for munging and transforming DataFrames\n",
    "I often use R’s dplyr package for exploratory data analysis and data manipulation. In addition to providing a consistent set of functions that one can use to solve the most common data manipulation problems, dplyr also allows one to write elegant, chainable data manipulation code using pipes.\n",
    "\n",
    "https://github.com/kieferk/dfply \n",
    "\n",
    "dplyr-style piping operations for pandas dataframes \n",
    "\n",
    "#### To install type: \n",
    "```pip install dfply```\n",
    "\n",
    "\n",
    "Importing the dfply library:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dfply import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The >> and >>= pipe operators\n",
    "\n",
    "dfply works directly on pandas DataFrames, chaining operations on the data with the >> operator, or alternatively starting with >>= for inplace operations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can chain piped operations, and of course assign the output to a new DataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lowprice = diamonds >> head(10) >> tail(3)\n",
    "lowprice"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Inplace operations are done with the first pipe as >>= and subsequent pipes as >>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lowprice >>= head(10) >> tail(2)\n",
    "lowprice"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The ```X``` DataFrame symbol\n",
    "\n",
    "The DataFrame as it is passed through the piping operations is represented by the symbol X. Selecting two of the columns, for example, can be done using the symbolic X DataFrame during the piping operations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> select(X.carat, X.cut) >> head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Selecting and dropping\n",
    "#### ```select()``` and ```drop()``` functions\n",
    "\n",
    "There are two functions for selection, inverse of each other: select and drop. The select and drop functions accept string labels, integer positions, and/or symbolically represented column names (X.column). They also accept symbolic \"selection filter\" functions, which will be covered shortly.\n",
    "\n",
    "The example below selects \"cut\", \"price\", \"x\", and \"y\" from the diamonds dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> select(X.cut, X.price, X.x, X.y) >> head(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you were instead to use drop, you would get back all columns besides those specified."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> drop(X.cut, X.price, X.x, X.y) >> head(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Selection using the inversion ~ operator on symbolic columns\n",
    "\n",
    "The inversion operator is an alternative in selecting certain columns of a dataframe.\n",
    "For example, let's say to select any column except carat, color, and clarity in the diamonds dataset use the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> select(~X.carat, ~X.color, ~X.clarity) >> head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Selection filter functions\n",
    "\n",
    "The vanilla select and drop functions are useful, but there are a variety of selection functions inspired by dplyr available to make selecting and dropping columns a breeze. These functions are intended to be put inside of the select and drop functions, and can be paired with the ~ inverter.\n",
    "\n",
    "First, a quick rundown of the available functions:\n",
    "\n",
    "* ```starts_with(prefix)```: find columns that start with a string prefix.\n",
    "* ```ends_with(suffix)```: find columns that end with a string suffix.\n",
    "* ```contains(substr)```: find columns that contain a substring in their name.\n",
    "* ```everything()```: all columns.\n",
    "* ```columns_between(start_col, end_col, inclusive=True)```: find columns between a specified start and end column. The inclusive boolean keyword argument indicates whether the end column should be included or not.\n",
    "* ```columns_to(end_col, inclusive=True)```: get columns up to a specified end column. The inclusive argument indicates whether the ending column should be included or not.\n",
    "* ```columns_from(start_col)```: get the columns starting at a specified column.\n",
    "\n",
    "The selection filter functions are best explained by example. For example, to select only the columns that started with a \"c\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> select(starts_with('c'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively the inversion opertor can be used select the columns that do not start with \"c\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> select(~starts_with('c'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Selection filters work the same inside the drop function, for example, use the columns_from selection filter to drop all columns from \"price\" onwards:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> drop(columns_from(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Subsetting and filtering\n",
    "```row_slice()```\n",
    "\n",
    "Slices of rows can be selected with the row_slice() function. You can pass single integer indices or a list of indices to select rows as with. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> row_slice([10,15,23])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### ```sample()```\n",
    "\n",
    "The ```sample()``` function functions exactly the same as pandas' .sample() method for DataFrames. Arguments and keyword arguments will be passed through to the DataFrame sample method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> sample(n=5, replace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### ```distinct()```\n",
    "\n",
    "Selection of unique rows is done with ```distinct()```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> distinct(X.color)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### ```mask()```\n",
    "\n",
    "Filtering rows with logical criteria is done with ```mask()```, which accepts boolean arrays \"masking out\" False labeled rows and keeping True labeled rows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> mask(X.cut == 'Ideal') >> head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### ```mutate()```\n",
    "\n",
    "New variables can be created with the ```mutate()``` function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> mutate(x_plus_y=X.x + X.y) >> head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple variables can be created in a single call."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> mutate(x_plus_y=X.x + X.y, y_div_z=(X.y / X.z)) >> head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### ```transmute()```\n",
    "\n",
    "The ```transmute()``` function is a combination of a mutate and a selection of the created variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> transmute(x_plus_y=X.x + X.y, y_div_z=(X.y / X.z)) >> head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Grouping\n",
    "```group_by()``` and ```ungroup()```\n",
    "\n",
    "DataFrames are grouped along variables using the group_by() function and ungrouped with the ungroup() function. Functions chained after grouping a DataFrame are applied by group until returning or ungrouping."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut, X.color)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reshaping\n",
    "`arrange()`\n",
    "\n",
    "Sorting is done by the `arrange()` function, which wraps around the pandas .sort_values() DataFrame method \n",
    "\n",
    "By default, the values will be arranged in ascending order"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> arrange(X.table, ascending=False) >> head(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(diamonds >> group_by(X.cut) >> arrange(X.price) >>\n",
    " head(3) >> ungroup() >> mask(X.carat < 0.23))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### rename()\n",
    "\n",
    "The ```rename()``` function will rename columns provided as values to what you set as the keys in the keyword arguments. You can indicate columns with symbols or with their labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> rename(CUT=X.cut, COLOR='color') >> head(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summarization\n",
    "\n",
    "There are two summarization functions in dfply\n",
    "\n",
    "```summarize(**kwargs)``` takes an arbitrary number of keyword arguments that will return new columns labeled with the keys that are summary functions of columns in the original DataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> summarize(price_mean=X.price.mean(), price_std=X.price.std())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```summarize()``` can of course be used with groupings as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by('cut') >> summarize(price_mean=X.price.mean(), price_std=X.price.std())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### summarize_each()\n",
    "\n",
    "The ```summarize_each(function_list, *columns)``` is a more general summarization function. It takes a list of summary functions to apply as its first argument and then a list of columns to apply the summary functions to. Columns can be specified with either symbolic, string label, or integer position like in the selection functions for convenience."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> summarize_each([np.mean, np.var], X.price, 'depth')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary functions\n",
    "\n",
    "#### mean()\n",
    "\n",
    "```mean(series)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_mean=mean(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### first()\n",
    "\n",
    "```first(series, order_by=None)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_first=first(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### last()\n",
    "\n",
    "```last(series, order_by=None)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_last=last(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### nth()\n",
    "\n",
    "```nth(series, n, order_by=None)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_penultimate=nth(X.price, -2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### n()\n",
    "\n",
    "```n(series)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_n=n(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### n_distinct()\n",
    "\n",
    "```n_distinct(series)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_ndistinct=n_distinct(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### IQR()\n",
    "\n",
    "IQR(series)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_iqr=IQR(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### colmin()\n",
    "\n",
    "```colmin(series)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_min=colmin(X.price))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### colmax()\n",
    "\n",
    "```colmax(series)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_max=colmax(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### median()\n",
    "\n",
    "```median(series)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_median=median(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### var()\n",
    "\n",
    "```var(series)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_var=var(X.price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### sd()\n",
    "\n",
    "```sd(series)```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diamonds >> group_by(X.cut) >> summarize(price_sd=sd(X.price))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
